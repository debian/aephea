aephea (12-248-4) UNRELEASED; urgency=medium

  * Use secure URI in Homepage field.
  * Set debhelper-compat version in Build-Depends.
  * Refer to specific version of license GPL-3+.
  * Use canonical URL in Vcs-Browser.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 16 Oct 2021 02:13:05 -0000

aephea (12-248-3) unstable; urgency=medium

  * Team upload.
  * d/copyright:
     - DEP5
     - Add additional copyright statement
  * cme fix dpkg-control
  * Moved packaging from SVN to Git
  * Convert from cdbs to dh
  * debhelper 11
  * d/watch:
     - version=4
     - Secure URI
     - cleanup
  * Fix spelling in long description
  * Remove duplicated manpage which breaks autoreconf generated makefile

 -- Andreas Tille <tille@debian.org>  Thu, 01 Feb 2018 14:46:06 +0100

aephea (12-248-2) unstable; urgency=low

  * debian/control: fix vcs tags
  * debian/control: add missing Breaks+Replaces: zoem-doc, to enable upgrades
    from squeeze to wheezy.  Thanks Andreas Beckmann.  (Closes: #694365)

 -- Joost van Baal-Ilić <joostvb@debian.org>  Sun, 25 Nov 2012 23:29:07 +0100

aephea (12-248-1) unstable; urgency=low

  * New upstream release 12-248.
  * No longer mangle upstream version number.
  * debian/copyright: fix typo. Thanks lintian.
  * debian/control: we honor policy 3.9.3 (no changes needed).
  * debian/watch: stricter, no longer match aephea-latest.tar.gz symlink.
  * debian/control: update my name.

 -- Joost van Baal-Ilić <joostvb@debian.org>  Wed, 05 Sep 2012 13:20:12 +0200

aephea (10.008-2) testing-proposed-updates; urgency=low

  * debian/control: add missing Breaks+Replaces: zoem-doc, to enable upgrades
    from squeeze to wheezy.  Thanks Andreas Beckmann.  (Closes: #694365)

 -- Joost van Baal-Ilić <joostvb@debian.org>  Mon, 26 Nov 2012 10:13:51 +0100

aephea (10.008-1) unstable; urgency=low

  * First upload (Closes: #620479).
  * debian/control: add Recommends: zoem.  The package zoem <= 08-248-1 ships
    base.zmm, man.zmm, faq.zmm, doc.zmm, ref.zmm and ctr.zmm.  Later zoem's
    do not.  We ship these files in /usr/share/aephea/{aephea,pud}/.  You
    might need to set ZOEMSEARCHPATH=/usr/share/aephea/pud:/usr/share/aephea
    to use this aephea with zoem 10-265-1.
  * debian/README.Debian, debian/{pre,post}{inst,rm}: removed (empty template).
  * debian/doc-base.pud-faq: fix typo.

 -- Joost van Baal <joostvb@debian.org>  Thu, 12 May 2011 01:49:18 +0200

aephea (10.008-0.3) unstable; urgency=low

  * Non-public release.
  * debian/control: fix tags vcs-*.
  * debian/control: we honor policy 3.9.1 (was 3.8.4, no changes needed).
  * debian/doc-base.*: fix syntax.

 -- Joost van Baal <joostvb@debian.org>  Sat, 23 Apr 2011 02:14:35 +0200

aephea (10.008-0.2) unstable; urgency=low

  * Non-public release.
  * debian/control: architecture: s/any/all/.
  * debian/{rules,docs}: moved usr/share/aephea/{doc,examples} to u/s/doc/aephea
    (reported upstream).
  * debian/aephea.doc-base: first shot at adding some content.

 -- Joost van Baal <joostvb@debian.org>  Sun, 03 Apr 2011 08:37:56 +0200

aephea (10.008-0.1) unstable; urgency=low

  * Initial non-public release.

 -- Joost van Baal <joostvb@debian.org>  Sun, 03 Apr 2011 08:37:56 +0200
